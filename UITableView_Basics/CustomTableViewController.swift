//
//  CustomTableViewController.swift
//  UITableView_Basics
//
//  Created by Mac Mini on 9/14/16.
//  Copyright © 2016 Percoid. All rights reserved.
//

import UIKit

class CustomTableViewController: UIViewController {

    @IBOutlet weak var customTable: UITableView!
    
    var myImageArray = [UIImage(named: "lucy"),UIImage(named: "47 ronin"), UIImage(named: "san andreas"), UIImage(named: "Book of eli")]
    var myImageList = ["LUCY", "47 RONIN", "SAN ANDREAS", "BOOK OF ELI"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customTable.dataSource = self
        customTable.delegate = self

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}

extension CustomTableViewController : UITableViewDataSource {
   
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return myImageArray.count
        
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cellidentifier = "customCell"
        
        let cell = tableView.dequeueReusableCellWithIdentifier(cellidentifier, forIndexPath: indexPath) as! CustomCellTableView
        
        cell.imageviewCell.image = myImageArray[indexPath.row]
        cell.labelCell.text = myImageList[indexPath.row]
        
        return cell
    }
    

    
}

extension CustomTableViewController : UITableViewDelegate {
    
    
}