//
//  MyFavMovieVC.swift
//  UITableView_Basics
//
//  Created by Mac Mini on 9/19/16.
//  Copyright © 2016 Percoid. All rights reserved.
//

import UIKit

class MyFavMovieVC: UIViewController {
    
    var movieitems: NSMutableArray!
    
    @IBOutlet weak var movietable: UITableView!
     override func viewDidLoad() {
        super.viewDidLoad()
        self.movieitems = NSMutableArray()
        
//        let movieitem = MovieItem()
//        movieitem.movieName = "lucy"
//        movieitem.movieName = "Conjuring"
//        movieitem.movieName = "Transformer"
//        movieitem.movieName = "san andreas"
//        movieitem.completed = false
//        movieitems.addObject(movieitem)
          loadinitdata()
        
        
      
    }
    
  
    
   
  
   
    func loadinitdata() {
       
        
        for _ in 1...2 {
            let movieitem = MovieItem()
            movieitem.movieName = "lucy"
        
            movieitem.completed = false
            movieitems.addObject(movieitem)
        }
            
        
    }
  
    @IBAction func unwindToList(segue: UIStoryboardSegue) {
        let source=segue.sourceViewController as! AddFavMovieVC
        let item = source.movieitem
        if(item != nil)
        {
            self.movieitems.addObject(item)
            self.movietable.reloadData()
        
        }
    }
    
    



}


extension MyFavMovieVC: UITableViewDataSource {
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return movieitems.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cellidentifier = "movieCell"
        let cell = tableView.dequeueReusableCellWithIdentifier(cellidentifier, forIndexPath: indexPath)
        let items = self.movieitems.objectAtIndex(indexPath.row) as! MovieItem
      //  cell.textLabel?.text = items.movieName
//            cell.movielabel!.text = items.movieName
        cell.textLabel?.text = items.movieName
        if(items.completed == true){
            cell.accessoryType = UITableViewCellAccessoryType.Checkmark
        } else{
            cell.accessoryType = UITableViewCellAccessoryType.None
        }
        return cell
        
    }
    
    
  
    
}


extension MyFavMovieVC: UITableViewDelegate {
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        let movieitem = self.movieitems.objectAtIndex(indexPath.row) as! MovieItem
        if(movieitem.completed == true) {
            movieitem.completed = false
        } else {
            movieitem.completed = true
        }
        
        tableView.reloadRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Left)
        
    }
}
