//
//  ArrayDataViewController.swift
//  UITableView_Basics
//
//  Created by Mac Mini on 9/14/16.
//  Copyright © 2016 Percoid. All rights reserved.
//

import UIKit

class ArrayDataViewController: UIViewController {
    
    var MyArray = ["Sunday", "Monday","Tuesday","Wednesday","Thrusday","Friday","Saturday"]
    var count = [0,1,2,3,4,5,6]

    override func viewDidLoad() {
        super.viewDidLoad()

        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}

extension ArrayDataViewController : UITableViewDataSource {
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         return MyArray.count
        
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        return 7;
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        //let array = tableView.dequeueReusableCellWithIdentifier("arrayCell", forIndexPath:indexpath )
        return "SECTION  \(section)"
    }
    
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cellidentifier = "arrayCell"
        
        let cell = tableView.dequeueReusableCellWithIdentifier(cellidentifier, forIndexPath: indexPath) as UITableViewCell
        cell.textLabel?.text  = MyArray[indexPath.row]
        return cell
        
    }

}

extension ArrayDataViewController : UITableViewDelegate {
    
}
