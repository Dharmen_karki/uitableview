//
//  personalDetailCell.swift
//  UITableView_Basics
//
//  Created by Mac Mini on 9/23/16.
//  Copyright © 2016 Percoid. All rights reserved.
//

import UIKit

class personalDetailCell: UITableViewCell {

    @IBOutlet weak var firstname: UILabel!
    @IBOutlet weak var lastname: UILabel!
    @IBOutlet weak var age: UILabel!
    
    @IBOutlet weak var streetaddress: UILabel!
    @IBOutlet weak var city: UILabel!
    @IBOutlet weak var state: UILabel!
    @IBOutlet weak var postalcode: UILabel!
    
    @IBOutlet weak var phonetype: UILabel!
    @IBOutlet weak var phonenumber: UILabel!
    
    
}
