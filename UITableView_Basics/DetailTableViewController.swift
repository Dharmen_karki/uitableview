//
//  DetailViewController.swift
//  UITableView_Basics
//
//  Created by Mac Mini on 9/14/16.
//  Copyright © 2016 Percoid. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {

    @IBOutlet weak var DetailTable: UITableView!

    @IBOutlet weak var tableview: UITableView!
    
    var imagename = ["lucy","47 ronin","san andreas","Book of eli"]
    var myImageArray = [UIImage(named: "lucy"),UIImage(named: "47 ronin"), UIImage(named: "san andreas"), UIImage(named: "Book of eli")]
    var myImageList = ["LUCY", "47 RONIN", "SAN ANDREAS", "BOOK OF ELI"]
    var movieDirector = ["Luc Besson"," Carl Rinsch","Brad Peyton","Albert Hughes, Allen Hughes"]
    var movieRelease = ["July 25, 2014 (USA)","august 20,2014","October 16,2015","November 25 2012"]
    var movieRating = ["3.5","3.4","3.8","3.7"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
      animateTable()

        // Do any additional setup after loading the view.
    }
    
    func animateTable() {
        
        self.tableview.reloadData()
        
        let cells  = tableview.visibleCells
        let tableHeight: CGFloat = tableview.bounds.size.height
        
        for i in cells {
            let cell: UITableViewCell = i as UITableViewCell
            cell.transform = CGAffineTransformMakeTranslation(0, tableHeight)
        }
        
        var index = 0
        
        for a in cells {
            
            let cell: UITableViewCell = a as UITableViewCell
            UIView.animateWithDuration(1.5, delay: 0.05 * Double(index), usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: [], animations: {
                cell.transform = CGAffineTransformMakeTranslation(0, 0)
                }, completion: nil)
            index += 1
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func goDetailBtn(sender: AnyObject) {
       let  detailVC = self.storyboard?.instantiateViewControllerWithIdentifier("detailStoryboard")
        //self.presentViewController(detailVC!, animated: true, completion: nil)
        self.navigationController?.pushViewController(detailVC!, animated: true)
        
    }

//    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
//        if(segue.identifier == "gotoDetail"){
//            
//            let destinationVC = segue.destinationViewController as! DetailViewControllerr
//            destinationVC.image = UIImage(named: "lucy")
//            destinationVC.directorr = "amfamdsl"
//            
//        }
//    }

}

extension DetailViewController : UITableViewDataSource {
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    
        return myImageArray.count
    }
    
   
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("detailCell", forIndexPath: indexPath) as! DetailCustomViewCell
        cell.detailimage.image = myImageArray[indexPath.row]
        cell.detaillabel.text = myImageList[indexPath.row]
        return cell
        
    }
    

    
}


extension DetailViewController : UITableViewDelegate {
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath){
        
        //let cell = tableView.dequeueReusableCellWithIdentifier("detailCell", forIndexPath: indexPath) as! DetailCustomViewCell
        
        let selectedcell = tableView.cellForRowAtIndexPath(indexPath) as! DetailCustomViewCell
        print(selectedcell.detailTextLabel?.text)
        print("\(myImageList[indexPath.row])")
        
        //let image = myImageArray[indexPath.row]
        
        
        let view = self.storyboard?.instantiateViewControllerWithIdentifier("detailStoryboard") as! DetailViewControllerr
        view.mymovie = myImageArray
        print(myImageArray)
        //self.presentViewController(view!, animated: true, completion: nil)
        view.imagedetail = imagename[indexPath.row]
        view.movienameedetail = myImageList[indexPath.row]
        view.directordetail = movieDirector[indexPath.row]
        view.movieReleaseDetail = movieRelease[indexPath.row]
        view.movieRatingDetail = movieRating[indexPath.row]
        //view.detailimageView.image = myImageArray[indexPath.row]
        self.navigationController?.pushViewController(view, animated: true)
        
        
    }
}