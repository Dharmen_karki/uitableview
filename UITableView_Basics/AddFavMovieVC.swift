//
//  AddFavMovieVC.swift
//  UITableView_Basics
//
//  Created by Mac Mini on 9/19/16.
//  Copyright © 2016 Percoid. All rights reserved.
//

import UIKit

class AddFavMovieVC: UIViewController {
    
    @IBOutlet weak var doneButton: UIBarButtonItem!
    @IBOutlet weak var movietxtfield: UITextField!
    
    var movieitem : MovieItem!
    
   override func viewDidLoad() {
        super.viewDidLoad()
    
    }
    
    

  
    
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if((sender as! UIBarButtonItem) != self.doneButton){
            return
        }
        
        if(movietxtfield.text != nil) {
            movieitem = MovieItem()
            movieitem.movieName = self.movietxtfield.text
            movieitem.completed = false
        }
        
    }
}
