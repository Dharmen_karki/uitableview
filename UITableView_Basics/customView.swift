//
//  customView.swift
//  UITableView_Basics
//
//  Created by Mac Mini on 9/16/16.
//  Copyright © 2016 Percoid. All rights reserved.
//

import UIKit

 class customView: UIView {
    
    var view: UIView!
    func xibSetup() {
        
        view = loadViewFromNib()
        
        view.frame = bounds
        
        view.autoresizingMask = [UIViewAutoresizing.FlexibleWidth, UIViewAutoresizing.FlexibleHeight]
        
        addSubview(view)
        
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
      func loadViewFromNib() -> UIView {
        let views = UINib(nibName: "customView", bundle: nil).instantiateWithOwner(self, options: nil).first as! UIView;
//        let viewsss  = NSBundle.mainBundle().loadNibNamed("customView", owner: self, options: nil);
////    
//            let bundle = NSBundle(forClass)
//            let nib = UINib(nibName: customView, bundle: bundle)
//            let view = nib.instantiateWithOwner(self, option : nil) [0] as! UIView
        
            return views
    }

}
