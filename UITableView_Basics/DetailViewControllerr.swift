//
//  DetailViewControllerr.swift
//  UITableView_Basics
//
//  Created by Mac Mini on 9/14/16.
//  Copyright © 2016 Percoid. All rights reserved.
//

import UIKit

class DetailViewControllerr: UIViewController {

    
    @IBOutlet weak var movierating: UILabel!
    @IBOutlet weak var director: UILabel!
    @IBOutlet weak var initialrelease: UILabel!
    @IBOutlet weak var moviename: UILabel!
    @IBOutlet weak var detailimageView: UIImageView!
    
    var imagedetail : String!
    var directordetail : String?
    var movienameedetail : String?
    var movieReleaseDetail : String?
    var movieRatingDetail : String?
    
    var mymovie: [UIImage?]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let abc = mymovie {
        
                print(abc)}
        //print(mymovie)
        self.detailimageView.image = UIImage(named: imagedetail)
        self.moviename.text = movienameedetail
        self.director.text = directordetail
        self.initialrelease.text = movieReleaseDetail
        self.movierating.text = movieRatingDetail
      
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}
