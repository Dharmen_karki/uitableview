//
//  Animate cell load json file.swift
//  UITableView_Basics
//
//  Created by Mac Mini on 9/23/16.
//  Copyright © 2016 Percoid. All rights reserved.
//

import UIKit

class Animate_cell_load_json_file: UIViewController {
    

    
    @IBOutlet weak var tableview: UITableView!
    
    var firstname = " "
    var lastname = " "
    var age = 0
    var address = [String]()
    var city = " "
    var street = " "
    var state = " "
    var postalcode = " "
    var phonetype = [String]()
    var phonenumber = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadJsonfile()
        
    }
    
    override func viewWillAppear(animated: Bool) {
        animateTable()
    }
    
    func animateTable() {
        
        self.tableview.reloadData()
        
        let cells  = tableview.visibleCells
        let tableHeight: CGFloat = tableview.bounds.size.height
        
        for i in cells {
            let cell: UITableViewCell = i as UITableViewCell
            cell.transform = CGAffineTransformMakeTranslation(0, tableHeight)
        }
        
        var index = 0
        
        for a in cells {
            
            let cell: UITableViewCell = a as UITableViewCell
            UIView.animateWithDuration(1.5, delay: 0.05 * Double(index), usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: [], animations: {
                cell.transform = CGAffineTransformMakeTranslation(0, 0)
                }, completion: nil)
            index += 1
        }
    }
    
    
    func loadJsonfile() {
        
        let path = NSBundle.mainBundle().pathForResource("Json_03", ofType: "json")
        let data = NSData(contentsOfFile: path!)
        
        do{
            
            let json = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.AllowFragments)
            
            
            if let firstname = json["firstName"] as? String {
                if let lastname = json["lastName"] as? String{
                    if let age = json["age"] as? Int{
                        print(firstname, lastname, age)
                        self.firstname = firstname
                        self.lastname = lastname
                        self.age = age
                        
                    }
                }
            }
        
          
            if let address = json["address"] as? NSDictionary {
                if let streetaddress = address["streetAddress"]{
                    if let city = address["city"]{
                        if let state = address["state"]{
                            if let postalcode = address["postalCode"]{
                                print(streetaddress, city, state, postalcode)
                                self.street = streetaddress as! String
                                self.city = city as! String
                                self.postalcode = postalcode as! String
                                self.state = state as! String
                            }
                        }
                    }
                }
            }
            
            if let personalInfo = json["phoneNumber"] as? [[String: AnyObject]] {
                for phoneInfo in personalInfo {
                    if let type = phoneInfo["type"] as? String {
                        if let number = phoneInfo["number"] as? String{
                        print(type,number)
                            self.phonetype.append(type)
                            self.phonenumber.append(number)
                    }
                }
            }
        }
        } catch {
            print("error with json \(error)")
        }

}
}


extension Animate_cell_load_json_file : UITableViewDataSource {
    

    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return phonenumber.count
        
        
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("personalDetailCell", forIndexPath: indexPath) as! personalDetailCell
        cell.firstname.text = firstname
        cell.lastname.text = lastname
        cell.streetaddress.text = street
        cell.city.text = city
        cell.postalcode.text = postalcode
        cell.state.text = state
        cell.phonetype.text = phonetype[indexPath.row]
        cell.phonenumber.text = phonenumber[indexPath.row]
        
        return cell
        
    }
    
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 5
    }
    
}

extension Animate_cell_load_json_file : UITableViewDelegate{
    
//    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
//        
//        if let cell = tableview.cellForRowAtIndexPath(indexPath) as? personalDetailCell {
//            
//            UIView.animateWithDuration(0.5, delay: 0.2, options: .CurveEaseIn, animations:{ () -> Void in
//                cell.backgroundColor = UIColor.blackColor()
//                }, completion: nil)
//            
//            UIView.animateWithDuration(0.5, delay: 0.2,  options: .CurveEaseOut, animations: {() -> Void in
//                self.firstnamelbl.textColor = UIColor(red:0.959, green:0.937, blue:0.109, alpha:1)
//                }, completion: nil)
//        }
//        
//        
//        
//        
//    }
}
