//
//  StaticDataViewController.swift
//  UITableView_Basics
//
//  Created by Mac Mini on 9/14/16.
//  Copyright © 2016 Percoid. All rights reserved.
//

import UIKit

class StaticDataViewController: UIViewController {

    @IBOutlet weak var statictableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

       statictableView.dataSource = self
       statictableView.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}


extension StaticDataViewController : UITableViewDataSource {
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 10
    }
   
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cellidentifier = "staticCell"
        let cell = tableView.dequeueReusableCellWithIdentifier(cellidentifier, forIndexPath: indexPath) as UITableViewCell
        if (indexPath.row % 2 == 0){
        cell.textLabel?.text = "Hello i m static even cell"
        cell.backgroundColor = UIColor.blackColor()
        cell.textLabel?.textColor = UIColor.whiteColor()
        
        } else {
            cell.textLabel?.text = "Hello i m static odd cell"
            cell.backgroundColor = UIColor.whiteColor()
            cell.textLabel?.textColor = UIColor.blackColor()
        }
        return cell
    }

}


extension StaticDataViewController : UITableViewDelegate {
    
    
}
