//
//  ProtocolDataXchangeViewController.swift
//  UITableView_Basics
//
//  Created by Mac Mini on 9/15/16.
//  Copyright © 2016 Percoid. All rights reserved.
//

import UIKit

protocol  exchangeDataDelegate {
    func accept(data: AnyObject!)

}

class ProtocolDataXchangeViewController: UIViewController {

    @IBOutlet weak var protocolTable: UITableView!
    
    var delegate : exchangeDataDelegate!
    
    
    var arrrayList = ["The Godfather","The Dark Knight","Schindler's List","12 Angry Men","Pulp Fiction","The Lord Of The Rings: The Return Of The King","The Good, The Bad And The Ugly","Fight Club"]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        protocolTable.dataSource = self
        protocolTable.delegate = self

    }
    
//   override func viewWillAppear(animated: Bool) {
//        super.viewWillAppear(animated)
//    if self.isBeingDismissed() {
//        self.delegate?.accept()
//    }
//    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    

}

extension ProtocolDataXchangeViewController : UITableViewDataSource {
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arrrayList.count
        
    }
   
     func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("protocolCell", forIndexPath: indexPath)
        cell.textLabel?.text = arrrayList[indexPath.row]
        cell.backgroundColor = UIColor.brownColor()
        cell.textLabel?.textColor = UIColor.cyanColor()
        cell.textLabel?.textAlignment = .Center
        return cell
        
    }
    
//    override func viewWillAppear(animated: Bool) {
//        super.viewWillAppear(animated)
//        self.delegate.accept()
//    }
//    

    
    
}

extension ProtocolDataXchangeViewController : UITableViewDelegate {
    
   func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    
    let selectedrow = tableView.cellForRowAtIndexPath(indexPath)
    let rowdata = selectedrow?.textLabel?.text
    self.delegate?.accept(rowdata)
    print(rowdata)
    
    }
    
}
